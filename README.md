# Si473x I2S module

Andrew Wyatt | [retrojdm.com](http://retrojdm.com/)

I've had a few made by JLCPCB, and successfully tested them in both analog and digital audio mode. See this [example Arduino sketch](./arduino-sketch/teensy32-Si473x-usb-audio/teensy32-Si473x-usb-audio.ino) as a starting point.

[![Si473x I2S module - photo](./docs/Si473x-i2s-module-thumb.jpg)](./docs/Si473x-i2s-module.jpg)

This repo contains four main [KiCad](https://www.kicad.org/) assets:

|                                                                                                                                                                                                                                     | Asset type | Description                             |
| :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :--------- | :-------------------------------------- |
| [![Si473x I2S module - KiCad Schematic](./docs/project-schematic-thumb.png)](./docs/project-schematic-screenshot.png) [![Si473x I2S module - KiCad Project](./docs/project-3dview-thumb.jpg)](./docs/project-3dview-screenshot.jpg) | Project    | PCB source files for 11 x 11 mm module  |
| [![Si473x I2S module - KiCad Symbol](./docs/symbol-thumb.png)](./docs/symbol-screenshot.png)                                                                                                                                        | Symbol     | for using this module in other projects |
| [![Si473x I2S module - KiCad Footprint](./docs/footprint-thumb.png)](./docs/footprint-screenshot.png)                                                                                                                               | Footprint  | for using this module in other projects |
| [![Si473x I2S module - KiCad Project](./docs/adapter-3dview-thumb.jpg)](./docs/adapter-3dview-screenshot.jpg)                                                                                                                       | Project    | PCB source files for breadboard adapter |

The module KiCad project also includes custom symbols and footprints for the Si473x, castellated holes "connector", and oscillator. They're configured as Project Specific Libraries with the nickname: "\_project".

## 11x11mm Module Pinout

| Pin | Name           | Description                                                                                                                                |
| --: | :------------- | :----------------------------------------------------------------------------------------------------------------------------------------- |
|   1 | SDIO           | I<sup>2</sup>C Data                                                                                                                        |
|   2 | SCLK           | I<sup>2</sup>C Clock                                                                                                                       |
|   3 | RFGND          | Used in different AM antenna configurations. See _2.1. QFN Typical Application Schematic_ in datasheet                                     |
|   4 | AMI            | AM Antenna Input                                                                                                                           |
|   5 | V<sub>D</sub>  | Digital and I/O supply voltage. 1.62V to 3.6V                                                                                              |
|   6 | #RST           | Reset (active low)                                                                                                                         |
|   7 | #SEN           | Serial Enable. Used for I<sup>2</sup>C address selection. LOW = 0x11. HIGH = 0x63.                                                         |
|   8 | NC             | Not Connected                                                                                                                              |
|   9 | V<sub>A</sub>  | Analog supply voltage. 2.7V to 5.5V                                                                                                        |
|  10 | GND            | Ground.                                                                                                                                    |
|  11 | ROUT/\[DOUT\]  | Right audio line output for analog output mode                                                                                             |
|  12 | LOUT/\[DFS\]   | Left audio line output for analog output mode                                                                                              |
|  13 | DOUT           | Digital output data for digital output mode (I2S OUT)                                                                                      |
|  14 | FMI            | FM RF inputs. FMI should be connected to the antenna trace                                                                                 |
|  15 | DFS            | Digital frame synchronization input for digital output mode (I2S LRCLK)                                                                    |
|  16 | GPIO3/\[DCLK\] | General purpose output, or digital bit synchronous clock input in digital output mode (I2S BCLK)                                           |
|  17 | GPIO2/\[#INT\] | General purpose output or interrupt pin. Must be LOW on reset to select I<sup>2</sup>C Mode. See _4.19 Control Interface_ in the datasheet |
|  18 | GPIO1          | General purpose output. Must be HIGH on reset to select I<sup>2</sup>C Mode. See _4.19 Control Interface_ in the datasheet                 |

## Module features

### Pin-compatible with common modules

`SDIO`, `SCLK`, `VD`, `GND`, `LOUT`, `ROUT`, and `FMI` pins are in the same positions as on the common TEA5767 and RDA5807 modules, meaning:

- if you use this Si473x module in analog audio mode, the TEA5767 and RDA5807 modules will be drop-in replacements for it in terms of PCB design, but
- this module is **not** a drop-in replacement for the TEA5767 and RDA5807 modules (because it still needs at least the `RST`, `SEN`, and `VA` pins connected)
- **Note:** All three modules still require different code. [pu2clr/SI4735](https://github.com/pu2clr/SI4735) supports both analog and I2S mode on the Si473x, while [mathertel/Radio](https://github.com/mathertel/Radio) is great for the TEA5767 and RDA5807M modules

### On-board crystal oscillator

Includes a 32.768kHz oscillator that provides a square wave signal to `RCLK` (which is not broken out).

This frees up `GPO3` to be used as `DCLK` for I2S digital audio out.

### I2S pins are broken out

The digital audio interface operates in slave mode and supports a variety of MSB-first audio data formats including I2S and left-justified modes.

- `DCLK` = Continuous serial clock (SCK). Typically called bit clock (BCLK)
- `DFS` = Word select (WS). Typically called left-right clock (LRCLK) or frame sync (FS)
- `DOUT` = Serial data (SD), but can be called SDATA, SDOUT, ADCDAT, etc.

**Note**: Master clock is not part of the I2S standard, and not used by the Si473x. See the [I2S wikipedia page](https://en.wikipedia.org/wiki/I%C2%B2S) for more info.

See _4.7 Digital Audio Interface_ in the datasheet.

### AM antenna

The AMI and RFGND pins are broken out directly. No components have been added to the module circuit. This gives you the freedom to choose between the loopstick and air loop antenna configurations (or having no AM antenna at all). See _2.1. QFN Typical Application Schematic_ in the datasheet.

### 20QFN package

To keep everything within the 11 x 11 mm module footprint, this module uses the smaller 3x3mm 20QFN package (Si4735-D60-GM).

The downside is it's far harder to solder if you're trying to make this module yourself.

### LCSC part numbers for PCB Assembly

The KiCad project includes LCSC part numbers as a field in each symbol's properties. This means you can use an assembly service like JLCPCB instead of hand-soldering the tiny components. You'll need to generate:

- a BOM CSV file from the schematic, and
- a Component placement (.pos) CSV file for the board

See JLCPCB's instructions [here](https://support.jlcpcb.com/article/84-how-to-generate-the-bom-and-centroid-file-from-kicad).

**Note:** To use the script in their instructions, you'll probably need to [Install an XSLT processor](http://www.sagehill.net/docbookxsl/InstallingAProcessor.html). It's a bit of a pain. You have to dig around in multiple ZIP files to get various DLLs and EXEs.

## Alternative/substitute parts

You can use any of the IC's in the Si473x family. See _6. Ordering Guide_ in the datasheet.

| Part Number      | Description                                        |
| :--------------- | :------------------------------------------------- |
| SI4730-D60-GM(R) | AM/FM Broadcast Radio Receiver                     |
| SI4731-D60-GM(R) | AM/FM Broadcast Radio Receiver with RDS/RBDS       |
| SI4734-D60-GM(R) | AM/FM/SW/LW Broadcast Radio Receiver               |
| SI4735-D60-GM(R) | AM/FM/SW/LW Broadcast Radio Receiver with RDS/RBDS |

**Note:** The optional R suffix denotes tape-and-reel packaging, and are usually much cheaper.

## Downloads

Here are the files you'll need for a PCB Assembly order with JLCPCB:

- [Module Gerber files](./downloads/si4735-i2s-module-1.0.0.zip)
- [Module Bill Of Materials (BOM)](./downloads/si4735-i2s-module-1.0.0-bom.csv)
- [Module Component Position Layer (CPL)](./downloads/si4735-i2s-module-1.0.0-cpl.csv)
- [Adapter Gerber files](./downloads/si4735-i2S-module-breadboard-adapter-1.0.0.zip)

## Related links

- [Si4730/31/34/35-D60 datasheet (PDF)](https://www.skyworksinc.com/-/media/Skyworks/SL/documents/public/data-sheets/Si4730-31-34-35-D60.pdf)
- [AN332: Si47xx Programming Guide (PDF)](https://www.skyworksinc.com/-/media/Skyworks/SL/documents/public/application-notes/AN332.pdf)
- [PU2CLR Si4735 Arduino Library](https://github.com/pu2clr/SI4735)
- [PU2CLR Si4735 Arduino Library documentation](https://pu2clr.github.io/SI4735/extras/apidoc/html/index.html)
- [Example Arduino sketch](./arduino-sketch/teensy32-si473x-usb-audio/teensy32-si473x-usb-audio.ino)