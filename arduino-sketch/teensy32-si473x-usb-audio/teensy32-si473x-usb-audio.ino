// This is a bare-minimum test to get I2S digital audio from an Si473x AM/FM Radio IC using a PJRC Teensy 3.2, and stream it to a
// PC via USB. You'll need to select one of the Audio USB types when compiling/uploading.
//
// Note: To hear the audio on your PC, you may need to fiddle with "Recording" sound device settings to "listen to this device".
//
// | Teensy 3.2 Pin |     Si473x Pin | Notes                                                    |
// | -------------: | -------------: | :------------------------------------------------------- |
// |            GND |       GND (10) | Ground                                                   |
// |            Vin |        VA  (9) | Analog Voltage (5V)                                      |
// |           3.3V |        VD  (5) | Digital Voltage (3.3V)                                   |
// |              0 |       RST  (6) | Active low                                               |
// |              1 |      GPO1 (18) | HIGH when resetting reset to enable I2C                  |
// |              2 |  GPO2/INT (17) | LOW when resetting to enable I2C                         |
// |             9* | GPO3/DCLK (16) | Teensy 3.2 I2S BCLK                                      |
// |            13* |      DOUT (13) | Teensy 3.2 I2S RX                                        |
// |            18* |      SDIO  (1) | Teensy 3.2 I2C SDA (don't forget pull-up resistors)      |
// |            19* |      SCLK  (2) | Teensy 3.2 I2C SCL (don't forget pull-up resistors)      |
// |            23* |       DFS (15) | Teensy 3.2 I2S LRCLK                                     |
// |              - |       SEN  (7) | Use this to select the I2C address. LOW=0x11, HIGH=0x63  |
// |              - |       FMI (14) | A short jumper wire is all you should need as an antenna |
// |                |                |                                                          |
// |              - |      LOUT (12) | Optional left analog output to sanity-check              |
// |              - |      ROUT (11) | Optional right analog output to sanity-check             |
//
// Teensy 3.2 pins marked with an * can't be changed. These are the hardware I2C and I2S pins.

#include <Audio.h>

// See https://www.pjrc.com/teensy/gui/
//
// GUItool: begin automatically generated code
AudioInputI2S            i2sIn;           //xy=77,38
AudioOutputUSB           usbOut;           //xy=216,38
AudioConnection          patchCord1(i2sIn, 0, usbOut, 0);
AudioConnection          patchCord2(i2sIn, 1, usbOut, 1);
// GUItool: end automatically generated code

// See https://github.com/pu2clr/Si4735/
// At the time of writing this, I used version 2.1.7
#include <SI4735.h>

#define AM_FUNCTION 1
#define FM_FUNCTION 0

#define PIN_Si473x_RESET 0
#define PIN_Si473x_GPIO1 1
#define PIN_Si473x_GPIO2 2

SI4735 rx;

void setup()
{
  // Teensy uses USB. The 9600 baud rate is ignored.
  Serial.begin(9600);
  Serial.println("Si473x I2S Test.");
  
  AudioMemory(8);

  // Si473x
  pinMode(PIN_Si473x_RESET, OUTPUT);
  pinMode(PIN_Si473x_GPIO1, OUTPUT);
  pinMode(PIN_Si473x_GPIO2, OUTPUT);  
  digitalWrite(PIN_Si473x_RESET, HIGH); // Reset is active low. This is just getting it ready. rx.setup() does the resetting.
  digitalWrite(PIN_Si473x_GPIO1, HIGH); //
  digitalWrite(PIN_Si473x_GPIO2, LOW);  // At reset, GPIO1 and GPIO2 need to be HIGH and LOW respectively to enable I2C mode.
  
  // Look for the Si47XX I2C bus address
  int16_t Si473xAddr = rx.getDeviceI2CAddress(PIN_Si473x_RESET);
  if ( Si473xAddr == 0 ) {
    Serial.println("Si473X not found! Did you pull SDA and SCL high with 4.7K resistors?");
    Serial.flush();
    while (1);
  } else {
    Serial.print("The Si473x I2C address is 0x");
    Serial.println(Si473xAddr, HEX);
  }

  rx.setRefClock(32768);
  rx.setRefClockPrescaler(1);
  rx.setup(PIN_Si473x_RESET, -1, FM_CURRENT_MODE, SI473X_ANALOG_DIGITAL_AUDIO, XOSCEN_RCLK);

  // This seems to need to go before the I2S stuff.
  rx.setFM(8400, 10800, 9410, 10);

  //rx.digitalOutputSampleRate(44100);

  // OSIZE Dgital Output Audio Sample Precision (0=16 bits, 1=20 bits, 2=24 bits, 3=8bits).
  // OMONO Digital Output Mono Mode (0=Use mono/stereo blend ).
  // OMODE Digital Output Mode (0=I2S, 6 = Left-justified, 8 = MSB at second DCLK after DFS pulse, 12 = MSB at first DCLK after DFS pulse).
  // OFALL Digital Output DCLK Edge (0 = use DCLK rising edge, 1 = use DCLK falling edge)
  //rx.digitalOutputFormat(0 /* OSIZE */, 0 /* OMONO */, 0 /* OMODE */, 0 /* OFALL*/);

  rx.setVolume(63);
  rx.setFrequency(9410);

  Serial.print("Current Frequency: ");
  Serial.println(rx.getFrequency());
}

void loop()
{
}
